#-------------------------------------------------
#
# Project created by QtCreator 2016-03-12T10:54:05
#
#-------------------------------------------------

TARGET = QTVLCXExample

TEMPLATE = app

QT += widgets gui

CONFIG += c++11

SOURCES += main.cpp\
           mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

INCLUDEPATH += ../include

unix {
    target.path = /usr/lib
    INSTALLS += target
}

#####################################

VLC_ROOT_PATH = C:/dev/vlc-2.2.1
VLC_SDK = sdk

INCLUDEPATH += $${VLC_ROOT_PATH}/$${VLC_SDK}/include
INCLUDEPATH += $${VLC_ROOT_PATH}/$${VLC_SDK}/include/vlc/plugins

android {
    DEFINES += __MINGW32__

    LIBS += $${VLC_ROOT_PATH}/android_so/libvlcjni.so
}
else {
    LIBS += -L$${VLC_ROOT_PATH}
    LIBS += -llibvlc -llibvlccore
}

#####################################

CONFIG(debug, debug|release) {
    LIBS += -LD:\build\qtvlcx\build-QTVLCX-Desktop_Qt_5_5_1_MinGW_32bit-Debug\debug
}

else {
    LIBS += -LD:\build\qtvlcx\build-QTVLCX-Desktop_Qt_5_5_1_MinGW_32bit-Release\release
}
LIBS += -lQTVLCX

#ifndef _QTVLCX_ITEM_P_H
#define _QTVLCX_ITEM_P_H

#include "qtvlcxglobal.h"
#include "qtvlcxitem.h"

#include <QGraphicsItem>

namespace QTVLCX
{
    class Player;

    class ItemPrivate : public QObject
    {

            Q_OBJECT

        public:

            enum PaintMode
            {
                PaintModeQPainter,
                PaintModeTexture
            };
            Q_ENUM(PaintMode)

            explicit ItemPrivate();
            virtual ~ItemPrivate();

            void        closeMedia();

            void        attachPlayer();

            ////////////////////////////////////////////////////////////////////////

            PaintMode   m_paintMode;

            Player   *  m_player;

            bool        m_attached;

            Item    *   q_ptr;
            Q_DECLARE_PUBLIC(Item)
    };
}

#endif // _QTVLCX_ITEM_P_H

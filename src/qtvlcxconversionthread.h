#ifndef QTVLCXCONVERSIONTHREAD_H
#define QTVLCXCONVERSIONTHREAD_H

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QImage>
#include <QPixmap>

namespace QTVLCX
{
    class ConversionThread : public QThread
    {
            Q_OBJECT

        public:

            ConversionThread(QObject *parent = 0);
            ~ConversionThread();

        public slots:

            void    convert(const QImage & img);

            void    abort();
            void    reset();

        signals:

            void    converted(const QPixmap pixmap);
            void    consume();

        protected:

            void    run() Q_DECL_OVERRIDE;

        private:

            mutable QMutex      m_mutex;
            QWaitCondition      m_condition;

            bool                m_process;
            volatile bool       m_abort;

            QImage              m_img;
    };
}

#endif // QTVLCXCONVERSIONTHREAD_H

#include "qtvlcxdiscoveryagent.h"
#include "qtvlcxdiscoveryagent_p.h"

#include <vlc/vlc.h>

// for service discovery
//#include <vlc/libvlc_media_discoverer.h>

// for dshow and v4l2 devices

//#include <vlc/plugins/vlc_configuration.h>
//#include <vlc/plugins/vlc_plugin.h>

#ifdef Q_OS_WIN
#define LIBVLC_USE_PTHREAD_CANCEL
#include <vlc/plugins/vlc_common.h>
#include <vlc/plugins/vlc_modules.h>
#include <vlc/plugins/vlc_interface.h>
#endif

#include <QDebug>
#include <QDir>

QTVLCX::DiscoveryAgentPrivate::DiscoveryAgentPrivate()
    : m_vlcInstance(0)
{
}

////////////////////////////////////////////////////////////////////////////////

QTVLCX::DiscoveryAgent::DiscoveryAgent(QObject * parent)
    : QObject(parent),
      d_ptr(new QTVLCX::DiscoveryAgentPrivate)
{
    d_ptr->q_ptr = this;
}

QTVLCX::DiscoveryAgent::~DiscoveryAgent()
{
    this->stopDiscovery();
}

void QTVLCX::DiscoveryAgent::startDiscovery()
{
    Q_D(QTVLCX::DiscoveryAgent);

    this->stopDiscovery();

    // we need to create an instance of vlc before this will work, even though
    // none of the below functions take an vlc_instance *.

    const char * argv[] =
    {
        "--no-xlib",
        "--network-caching=500",
        "--no-video-title-show",
        "--disable-screensaver"

#ifndef Q_OS_MAC
        ,"--avcodec-hw=any"
#endif
    };

    int argc = sizeof(argv) / sizeof(*argv);
    d->m_vlcInstance = libvlc_new(argc, argv);


    ////////////////////////////////////////////////////////////////////////////
    /// THIS DOES SERVICES BUT WE DONT WANT THAT YET
    ////////////////////////////////////////////////////////////////////////////

    //QList<QString> types = {"upnp", "sap", "mediadirs", "windrive"};
    //
    //for (QString t : types)
    //{
    //    qDebug() << "QTVLCX: looking for " << t << " services";
    //    libvlc_media_discoverer_t * discoverer = libvlc_media_discoverer_new_from_name(d->m_vlcInstance, t.toLatin1());
    //    libvlc_media_list_t * list = libvlc_media_discoverer_media_list(discoverer);
    //
    //    int listSize = libvlc_media_list_count(list);
    //
    //    for (int i = 0; i < listSize; i++)
    //    {
    //        libvlc_media_t * media = libvlc_media_list_item_at_index(list, i);
    //        char * mrl = libvlc_media_get_mrl(media);
    //        libvlc_media_release(media);
    //
    //        qDebug() << "QTVLCX: media discovered: " << QString(mrl);
    //    }
    //
    //    if (listSize == 0)
    //    {
    //        qDebug() << "QTVLCX: no media discovered";
    //    }
    //
    //    libvlc_media_discoverer_release(discoverer);
    //}

    ////////////////////////////////////////////////////////////////////////////
    /// DSHOW
    ////////////////////////////////////////////////////////////////////////////

#ifdef Q_OS_WIN

    if (!module_exists("dshow"))
    {
        qWarning() << "QTVLCX : no dshow module in vlc configuration";

        size_t moduleCount;
        module_t ** moduleList = module_list_get(&moduleCount);

        for (size_t i = 0; i < moduleCount; i++)
        {
            module_t * mod = moduleList[i];
            QString name(module_get_name(mod, false));
            QString desc(module_get_name(mod, true));

            qDebug() << "QTVLCX found: " << name << " -> " << desc;
        }

        if (moduleCount == 0)
        {
            qWarning() << "QTVLCX no modules found at all";
        }
    }
    else
    {
        intf_thread_t * p_intf = 0;
        module_config_t * p_module_config = config_FindConfig(VLC_OBJECT(p_intf), "dshow-vdev");

        if (p_module_config != 0)
        {
            char **values, **texts;

            ssize_t count = config_GetPszChoices(VLC_OBJECT(p_intf),
                                                 p_module_config->psz_name,
                                                 &values,
                                                 &texts);

            if (count == 0)
            {
                qDebug() << "QTVLCX : found no dshow options";
            }

            for (ssize_t i = 0; i < count && texts; i++)
            {
                char * txt = texts[i];
                char * val = values[i];

                if (txt == NULL || val == NULL)
                {
                    continue;
                }

                QString desc = QString::fromUtf8(val);
                QString name = QString::fromUtf8(txt);

                if (!name.isEmpty() && name.toLower() != "none")
                {
                    QString url = "dshow:// :dshow-vdev=" + desc.replace(":", "\\:") + " :dshow-adev=  :live-caching=300";

                    qDebug() << "QTVLCX : found dshow option -> " << name << ": " << url;

                    emit this->sourceFound(url, name);
                }

#ifndef Q_CC_MSVC // dont know why the free fails
                if (txt != 0 && *txt != 0)
                {
                    free(txt);
                }

                if (val != 0 && *val != 0)
                {
                    free(val);
                }
#endif
            }

#ifndef Q_CC_MSVC
            free(texts);
            free(values);
#endif
        }
        else
        {
            qWarning() << "QTVLCX : failed to load dshow module config";
        }
    }

#elif defined(Q_OS_ANDROID)

    //const QStringList filter = {"*"};

    QStringList devList = QDir("/dev/").entryList(QDir::NoFilter);
    //devList.replaceInStrings(QRegExp("^"), "/dev/");

    for (const QString & str : devList)
    {
        QString url = "v4l2:// :v4l2-standard=" + str;

        qDebug() << "QTVLCX : found v4l2 option -> " << str << ": " << url;

        // commented out cos this doesnt work properly atm
        //emit this->sourceFound(url, str);
    }

    if (devList.isEmpty())
    {
        qWarning() << "QTVLCX: found no devices in discovery agent";
    }

#endif

#ifdef QT_DEBUG
    // some test stream urls -> 1 is a youtube url, 2 is the same url after decoding, 3 is something else
    emit this->sourceFound("https://youtu.be/g_WZncx-Baw", "youtu.be link");
    emit this->sourceFound("https://r4---sn-3jvh-ajte.googlevideo.com/videoplayback?gcr=gb&sver=3&id=o-AE-O9uV21LdczLa9AMgCGsG0BksqR7NjcRAfEYObZ0GP&fexp=9405988%2C9414875%2C9419452%2C9422596%2C9427378%2C9428398%2C9431012%2C9433096%2C9433221%2C9433946%2C9435526%2C9436834%2C9438327%2C9438662%2C9439433%2C9439580%2C9440031%2C9441347%2C9442254%2C9442424%2C9442426%2C9443436%2C9443502%2C9444702%2C9444906%2C9445513&signature=3ABB2BA4EC9A0BCC12D3521649B1A310FEAC10A1.9CE46ADF6A30DC438AE209B25AD76F51E5DF9B4D&requiressl=yes&ip=81.168.23.121&lmt=1471050952974668&dur=225.558&source=youtube&ratebypass=yes&key=yt6&ipbits=0&expire=1471624938&initcwndbps=685000&pl=17&mime=video%2Fmp4&itag=22&ms=au&ei=ieK2V7mLPInyWsrAi-gM&mv=m&mt=1471603231&sparams=dur%2Cei%2Cgcr%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cpl%2Cratebypass%2Crequiressl%2Csource%2Cupn%2Cexpire&upn=EuVyJRS6wFw&mn=sn-3jvh-ajte&mm=31", "Decoded youtu.be link");
    emit this->sourceFound("http://download.blender.org/peach/bigbuckbunny_movies/big_buck_bunny_480p_stereo.avi", "Big Buck Bunny .avi");
    emit this->sourceFound("https://www.youtube.com/watch?v=rYL7J1MecG0", "old school 10 second countdown");
#endif
}

void QTVLCX::DiscoveryAgent::stopDiscovery()
{
    Q_D(QTVLCX::DiscoveryAgent);

    if (d->m_vlcInstance != 0)
    {
        libvlc_release(d->m_vlcInstance);
        d->m_vlcInstance = 0;
    }
}

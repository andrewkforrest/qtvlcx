#ifndef _QTVLCX_GLOBAL_H
#define _QTVLCX_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(QTVLCX_LIBRARY)
#  define QTVLCX_SHARED_EXPORT Q_DECL_EXPORT
#else
#  define QTVLCX_SHARED_EXPORT Q_DECL_IMPORT
#endif

namespace QTVLCX
{
    enum State
    {
        Opening = 0,
        Buffering = 1,
        Stopped = 2,
        Playing = 3,
        Paused = 4,
        Error = 5
    };
}

#ifdef QT_DEBUG
#define QTVLC_TID QString("0x%1").arg((quintptr)QThread::currentThread(), QT_POINTER_SIZE * 2, 16, QChar('0'))
#else
#define QTVLC_TID QString()
#endif

#endif // _QTVLCX_GLOBAL_H

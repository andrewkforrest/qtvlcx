#-------------------------------------------------
#
# Project created by QtCreator 2016-03-12T10:54:05
#
#-------------------------------------------------

TARGET = QTVLCX

TEMPLATE = lib

DEFINES += QTVLCX_LIBRARY

QT += widgets gui opengl network

message()
message("----------------- QTVLCX -----------------")

include (qtcompilercheck.pri)

CONFIG += c++11

SOURCES += \
    src/qtvlcxitem.cpp \
    src/qtvlcxplayer.cpp \
    src/qtvlcxplayerthread.cpp \
    src/qtvlcxconversionthread.cpp \
    src/qtvlcxdiscoveryagent.cpp

HEADERS +=\
    include/qtvlcxitem.h \
    src/qtvlcxitem_p.h \
    include/qtvlcxglobal.h \
    include/qtvlcxplayer.h \
    src/qtvlcxplayerthread.h \
    src/qtvlcxconversionthread.h \
    include/qtvlcxdiscoveryagent.h \
    src/qtvlcxdiscoveryagent_p.h

INCLUDEPATH += include

unix {
    target.path = /usr/lib
    INSTALLS += target
}

#VLC_ROOT_PATH = C:/Buildbot/vlc-2.2.1-win32

macx {
    VLC_ROOT_PATH = /Applications/VLC.app/Contents/MacOS
    VLC_SDK = libvlc

    INCLUDEPATH += $${VLC_ROOT_PATH}/include

    # makes the vlc lib includes so that we expect them to be in the same relative folder (flat deployment)
    QMAKE_POST_LINK = install_name_tool -change @loader_path/lib/libvlc.5.dylib @rpath/libvlc.5.dylib $${DESTDIR}/libQTVLCX.dylib
    QMAKE_POST_LINK += ;install_name_tool -change @loader_path/lib/libvlccore.8.dylib @rpath/libvlccore.8.dylib $${DESTDIR}/libQTVLCX.dylib
}
else {

    contains(QMAKE_TARGET.arch, x86_64) {
        VLC_ROOT_PATH = $${BUILD_LIBRARIES_ROOT}/vlc-2.2.4-win64
    }
    else {
        VLC_ROOT_PATH = $${BUILD_LIBRARIES_ROOT}/vlc-2.2.4-win32
    }


    VLC_SDK = sdk
    INCLUDEPATH += $${VLC_ROOT_PATH}/$${VLC_SDK}/include
    INCLUDEPATH += $${VLC_ROOT_PATH}/$${VLC_SDK}/include/vlc/plugins
}

message(VLC ROOT $${VLC_ROOT_PATH})
message($${MOC_DIR})
message($${INCLUDEPATH})

android {
    contains(CURRENT_BUILD_OS, Windows_NT){
        DEFINES += __MINGW32__
    } else {
        INCLUDEPATH += /Applications/VLC.app/Contents/MacOS/include
    }
    LIBS += $${BUILD_LIBRARIES_ROOT}/vlc_binaries/src/main/jniLibs/armeabi-v7a/libvlc.so
    LIBS += $${BUILD_LIBRARIES_ROOT}/vlc_binaries/src/main/jniLibs/armeabi-v7a/libvlcjni.so
}
else {
    macx {
        LIBS += $${VLC_ROOT_PATH}/lib/libvlc.5.dylib
        LIBS += $${VLC_ROOT_PATH}/lib/libvlccore.8.dylib
        #LIBS += -llibvlc -llibvlccore
    }
    else {
        LIBS += -L$${VLC_ROOT_PATH}

        *-msvc* {
            #need to include path to libs in path
            LIBS += -L$${VLC_ROOT_PATH}/$${VLC_SDK}/lib
        }

        LIBS += -llibvlc -llibvlccore
    }
}

CONFIG(debug, debug|release):*-msvc* {
    QMAKE_CXXFLAGS += /Od
}
